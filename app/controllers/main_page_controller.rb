class MainPageController < ApplicationController
  def index
    @homeApi = HTTParty.get("http://vpad.in/api/vpad/portal/home/")
  end

  def login
    
    @loginApi = HTTParty.post("http://vpad.in/api/vpad/login",
                              :body => {  "lid" => params[:user][:email],
                                          "pwd" => params[:user][:password] }.to_json,
                              :headers => { 'Content-Type' => 'application/json'})
    if @loginApi.parsed_response == "OK"
      session[:access_token] = @loginApi.headers['x-access-token']
      redirect_to controller: 'main_page', action: 'index', :flash => { :success => "Message" }
    else
      redirect_to controller: 'main_page', action: 'index',:flash => {:error => "Not successfull"}
    end

  end

  def event_details
    eid = params[:event_details]

    @event_det = HTTParty.get("http://vpad.in/api/vpad/guide/#{eid}",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })
    @event_details = @event_det['data'].first
    @galleryApi = HTTParty.get("http://vpad.in/api/vpad/gallery",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })
     
    @abc = @galleryApi['data'].select {|t| t["eid"] == eid }

    @gallery = HTTParty.get("http://vpad.in/api/vpad/gallery/#{@abc.first['sid']}",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })
 
  end


  def clear_session
    reset_session
    redirect_to controller: 'main_page', action: 'index'
  end
  
  def user_profile
    @userinfoApi = HTTParty.get("http://vpad.in/api/vpad/userinfo",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })
    @user_details = @userinfoApi['data'].first

  end

  def regs
  
  end

  def new_user
    @registerApi = HTTParty.post("http://vpad.in/api/vpad/register",
                                 :body => {  "fname" => params[:new_user][:fname],"lname" => params[:new_user][:lname],"ph" => params[:new_user][:phone],
                                             "email" => params[:new_user][:email],"pwd" => params[:new_user][:password]}.to_json,
                                 :headers => { 'Content-Type' => 'application/json'})
    redirect_to controller: 'main_page', action: 'index'
  end

  def gallery
    eid = params[:eid]

    @galleryApi = HTTParty.get("http://vpad.in/api/vpad/gallery",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })
    @abc = @galleryApi['data'].select {|t| t["eid"] == eid }

    @gallery = HTTParty.get("http://vpad.in/api/vpad/gallery/#{@abc.first['sid']}",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })

  end

  def calendar
    @calendarApi = HTTParty.get("http://vpad.in/api/vpad/calendar",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })
    @calendar_type_hash = @calendarApi["data"].group_by{|item| item['type']}
  end

  def event_cal
    @calendarApi = HTTParty.get("http://vpad.in/api/vpad/calendar",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })
    @calendar_type_hash = @calendarApi["data"].group_by{|item| item['type']}
  end

  def vspecial_cal
    @calendarApi = HTTParty.get("http://vpad.in/api/vpad/calendar",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })
    @calendar_type_hash = @calendarApi["data"].group_by{|item| item['type']}
  end

  def festival_cal
    @calendarApi = HTTParty.get("http://vpad.in/api/vpad/calendar",:headers => {"X-Access-Token" => session[:access_token],'Content-Type' => 'application/json' })
    @calendar_type_hash = @calendarApi["data"].group_by{|item| item['type']}
  end

end
